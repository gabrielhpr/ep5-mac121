/*
  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__

  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE EP
  E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU RESPONSÁVEL
  POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO SÃO PUNIDOS COM 
  REPROVAÇÃO DIRETA NA DISCIPLINA.

  Nome: Gabriel Henrique Pinheiro Rodrigues
  NUSP: 112.216-47

  IMDB: filmes.c


  Referências: Com exceção das rotinas fornecidas no esqueleto e em sala
  de aula, caso você tenha utilizado alguma refência, liste-as abaixo
  para que o seu programa não seja considerada plágio.
  Exemplo:
  - função mallocc retirada de: http://www.ime.usp.br/~pf/algoritmos/aulas/aloca.html

  \__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__\__
*/


/*----------------------------------------------------------*/
/* filmes.h e a interface para as funcoes neste modulo      */
#include "filmes.h" 

/*----------------------------------------------------------*/
#include <stdlib.h>  /* NULL, free() */
#include <stdio.h>   /* printf(), scanf() */ 
#include <string.h>  /* strlen(), strncpy(), strcmp(), strtok() */

#include "util.h"    /* Bool, mallocSafe() */
#include "iofilmes.h"
#include "st.h"      /* freeST(), initST(), putFilmeST(), getFilmeST(),
                        showST(), freeST() */

/*----------------------------------------------------------------------
 *  crieFilme
 *
 *  Recebe informacoes dobre um filme 
 *
 *      - DIST  : distribuicao de notas
 *      - VOTOS : numero de votos
 *      - NOTA  : nota do filme 
 *      - ANO   : ano de producao do filme
 *
 *  e cria uma celula do tipo Filme para armazenar essa informacao. 
 *  A funcao retorna o endereco da celula criada.
 */
Filme *
crieFilme (char dist[], int votos, float nota, char *nome, int ano)
{
    Filme *flm;
    int    len = strlen(nome) + 1; /* +1 para o '\0' */
    
    flm = mallocSafe(sizeof *flm);
    
    strncpy(flm->dist, dist, TAM_DIST+1); /* +1 para o '\0' */
    
    flm->votos = votos;
    flm->nota  = nota;
    
    flm->nome = mallocSafe(len*sizeof(char));
    strncpy(flm->nome, nome, len);
    
    flm->ano  = ano;

    flm->prox = flm->ant = NULL; /* paranoia */
    
    return flm;
}

/*----------------------------------------------------------------------
 *  crieListaFilmes
 * 
 *  Cria uma estrutura que representa lista de filmes vazia.
 *  A funcao retorna o endereco dessa estrutura.
 *
 *  Um lista de filmes e representada por uma lista encadeada 
 *  duplamente ligada com cabeca. 
 */
ListaFilmes *
crieListaFilmes()
{
    ListaFilmes* lst = mallocSafe(sizeof(*lst));
    Filme* cabeca = mallocSafe(sizeof(*cabeca));
    cabeca->prox = cabeca;
    cabeca->ant = cabeca;
    lst->cab = cabeca;
    
    return lst;
}

/*----------------------------------------------------------------------
 *  libereListaFilmes(lst)
 *
 *  Recebe um ponteiro lst para uma estrutura que representa uma lista 
 *  de filmes e libera toda a memoria alocada para a lista.
 *
 *  Esta funcao utiliza a funcao libereFilme().
 */

void
libereListaFilmes(ListaFilmes *lst)
{
    Filme* aux = lst->cab->prox;
    while(aux != lst->cab) {
        aux = aux->prox;
        libereFilme(lst->cab->prox);
        lst->cab->prox = aux;
    }
}

/*----------------------------------------------------------------------
 *  libereFilme
 *
 *  Recebe um ponteiro FLM para uma estrutura que representa um 
 *  filme e libera a area alocada.
 *
 */
void 
libereFilme(Filme *flm)
{
    /*Libera o nome*/
    free(flm->nome);
    /*Libera a estrutura Filme*/
    free(flm);
}

/*----------------------------------------------------------------------
 *  insiraFilme
 *
 *  Recebe um ponteito LST para uma estrutura que representa
 *  uma lista de filmes e um ponteiro FLM para uma estrutura
 *  que representa uma filme.
 *
 *  A funcao insere o filme na lista.
 *  
 */
void 
insiraFilme(ListaFilmes *lst, Filme *flm)
{
    /*Insere o filme no início da lista*/
    flm->prox = lst->cab->prox;
    flm->ant = lst->cab;
    lst->cab->prox->ant = flm;
    lst->cab->prox = flm;
    lst->nFilmes += 1;
}


/*---------------------------------------------------------------------
 *  contemFilme
 *
 *  Recebe um ponteito LST para uma estrutura que representa
 *  uma lista de filmes e um ponteiro FLM para uma estrutura
 *  que representa uma filme.
 *
 *  A funcao retorna  TRUE se o filme esta na lista e 
 *  FALSE em caso contrario. 
 *
 *  Consideramos que dois filmes f e g sao iguais se
 *
 *       - f->nome e' igual a g->nome 
 *       - f->nota e' igual a g->nota
 *       - f->ano  e' igual a g->ano 
 *
 *  Para comparar dois nomes voce pode usar alguma funcao da 
 *  bibliteca do c  como strcmp, strncmp (string,h) 
 *  ou a funcao strCmp (util.h).
 *
 */
Bool 
contemFilme(ListaFilmes *lst, Filme *flm)
{
    Filme* aux = lst->cab->prox;
    int achou = FALSE;
    while(aux != lst->cab && !achou) {
        achou = TRUE;
        if(strcmp(flm->nome, aux->nome) != 0 || flm->nota != aux->nota || flm->ano != aux->ano) 
            achou = FALSE;
        aux = aux->prox;
    }
    return achou;
}

/*----------------------------------------------------------------------
 *  removaFilme
 *
 *  Remove da lista de filmes LST o filme apontado por FLM.
 *    
 *  Pre-condicao: a funcao supoe que o filme FLM esta 
 *                na lista LST.
 */
void 
removaFilme(ListaFilmes *lst, Filme *flm)
{
    Filme* anterior = flm->ant;
    Filme* posterior = flm->prox;
    anterior->prox = posterior;
    posterior->ant = anterior;
    libereFilme(flm);
}

/*----------------------------------------------------------------------
 *  mergeSortFilmes
 *
 *  Recebe uma lista de filmes LST e ordena a lista utilizando o
 *  algoritmo mergeSort recursivo adaptado para listas encadeadas
 *  duplamente ligadas com cabeca.
 *
 *  A funcao recebe ainda um parametro CRITERIO tal que:
 *
 *  Se CRITERIO == NOTA, entao a lista deve ser ordenada
 *      em ordem decrescente de nota.
 *
 *  Se CRITERIO == NOME, entao a lista deve ser ordenada
 *      em ordem crescente de nome (ordem alfabetica).
 *
 *  ------------------------------------------------------------------
 *  OBSERVACAO IMPORTANTE:
 *
 *  A ordenacao deve ser feita 'in-place', ou seja o conteudo das
 *  celulas _nao deve_ ser copiado, apenas os ponteiros devem ser
 *  alterados.
 *
 *  A funcao so deve utilizar espaco extra O(1).  
 *
 *  Hmmm, ok, sem levar em consideracao o espaco O(lg n) utilizado
 *  pela pilha da recursao.  Em outras palavras, a funcao pode conter
 *  apenas declaracoes de umas poucas variaveis (um vetor v[0..n]
 *  conta como n variaveis e nao vale).
 *
 *  ------------------------------------------------------------------
 *  Para ordenar por nome, veja a funcao strCmp em util.[h|c].
 */


void mergeSortFilmes(ListaFilmes* lst, Criterio criterio) {

    ListaFilmes *lst2;
    Filme *cab2;
    /*Armazena a posição da metade da lista*/
    Filme *metDaLista = lst->cab;
    /*índices das duas listas*/
    Filme *esq, *dir;
    /*Posições auxiliares*/
    Filme *esqAnt, *dirAnt, *dirProx;
    int i, totalDeFilmes;

    /*Guarda a quantidade total de filmes*/
    totalDeFilmes = lst->nFilmes;

    /*Base da recursão, só um elemento na lista*/
    if(lst->cab->prox == lst->cab->ant) {
        return;
    }
    lst2 = mallocSafe(sizeof(ListaFilmes));
    /*Cria uma cabeça secundária*/
    cab2 = mallocSafe(sizeof(Filme));
    /*lst2 aponta para a cabeça criada*/
    lst2->cab = cab2;

    /*Acha a metade da lista*/
    for (i = 0; i < (int) lst->nFilmes/2; i++) {
        metDaLista = metDaLista->prox;
    }

    /*Atualiza a quantidade de filmes - da metade da lista*/    
    lst->nFilmes = i;

    /*Atualiza a quantidade de filmes da outra metade da lista*/
    lst2->nFilmes = totalDeFilmes - i;

    /*Engloba os membros da lista da direita*/
    lst2->cab->prox = metDaLista->prox;
    lst2->cab->ant = lst->cab->ant;

    /*O elemento pós metade é apontado por cab2, e o seu ant aponta para ele*/
    metDaLista->prox->ant = lst2->cab;
    /*O último elemento aponta para a cabeça 2*/
    lst->cab->ant->prox = lst2->cab;

    /*Engloba os membros da esquerda, lista da esquerda*/
    lst->cab->ant = metDaLista;
    metDaLista->prox = lst->cab;

    /*Chama o mergesort na esquerda*/
    mergeSortFilmes(lst, criterio);
    /******************************/
    
    /*Chama o mergesort na direita*/
    mergeSortFilmes(lst2, criterio);
    /******************************/
    
    /*TROCA*/
    /*Percorre ambas as listas, e coloca os membros da lista da direita, na 
    sua posição correspondente na lista da esquerda*/
    esq = lst->cab->prox;
    dir = lst2->cab->prox;

    while(dir != lst2->cab) {
        if(criterio == NOTA) {
            if(esq->nota < dir->nota) {
                /*Guarda o endereço de posições auxiliares*/
                esqAnt = esq->ant;
                dirAnt = dir->ant;
                dirProx = dir->prox;

                /*Isola o da direita*/
                dir->ant->prox = dirProx;
                dir->prox->ant = dirAnt;

                /*Insere o elemento da direita na lista da esquerda*/
                dir->prox = esq;
                dir->ant = esqAnt;
                esq->ant->prox = dir;
                esq->ant = dir;
                    
                dir = dirProx;
            }
            else {
                esq = esq->prox;
            }
        }
        else if(criterio == NOME) {
            if(esq->nome[0] >= dir->nome[0]) {
                /*Guarda o endereço de posições auxiliares*/
                esqAnt = esq->ant;
                dirAnt = dir->ant;
                dirProx = dir->prox;

                /*Isola o da direita*/
                dir->ant->prox = dirProx;
                dir->prox->ant = dirAnt;

                /*Insere o elemento da direita na lista da esquerda*/
                dir->prox = esq;
                dir->ant = esqAnt;
                esq->ant->prox = dir;
                esq->ant = dir;
                    
                dir = dirProx;
            }
            else { 
                esq = esq->prox;
            }
        }
    }
    lst->nFilmes = totalDeFilmes;
    /*Libera o espaço extra utilizado*/
    free(lst2->cab);
    free(lst2);
}


/*----------------------------------------------------------------------
 *  quickSortFilmes [opcional]
 *
 *  Recebe uma lista de filmes LST e ordena a lista utilizando o
 *  algoritmo quickSort adaptado para listas encadeadas duplamente
 *  ligadas com cabeca.
 *
 *  A funcao recebe ainda um parametro CRITERIO tal que:
 * 
 *  Se CRITERIO == NOTA, entao a lista deve ser ordenada
 *      em ordem decrescente de nota.
 *
 *  Se CRITERIO == NOME, entao a lista deve ser ordenada
 *      em ordem crescente de nome (ordem alfabetica).
 *
 *  ------------------------------------------------------------------
 *  OBSERVACAO IMPORTANTE:
 *
 *  A ordenacao deve ser feita 'in-place', ou seja o conteudo das
 *  celulas _nao deve_ ser copiado, apenas os ponteiros devem ser
 *  alterados.
 *
 *  A funcao so deve utilizar espaco extra O(1).  
 *
 *  Hmmm, ok, sem levar em consideracao o espaco O(lg n) utilizado
 *  pela pilha da recursao.  Em outras palavras, a funcao pode conter
 *  apenas declaracoes de umas poucas variaveis (um vetor v[0..n]
 *  conta como n variaveis e nao vale).
 *
 *  ------------------------------------------------------------------
 *  Para ordenar por nome, veja a funcao strCmp em util.[h|c].
 */
void 
quickSortFilmes(ListaFilmes *lst, Criterio criterio)
{
    AVISO(quickSortFilmes em filmes.c: Vixe ainda nao fiz essa funcao...);
}

/*----------------------------------------------------------------------
 *  hashFilmes [opcional]
 *
 *  Recebe uma lista de filmes LST e distribui as palavras que
 *  ocorrem nos nomes do filmes em uma tabela de dispersao 
 *  (hash table):
 *
 *     http://www.ime.usp.br/~pf/mac0122-2002/aulas/hashing.html
 *     http://www.ime.usp.br/~pf/mac0122-2003/aulas/symb-table.html
 *
 *  Antes de inserir uma palavra na tabela de dispersao todas a
 *  letras da palavra devem ser convertidas para minusculo. Com
 *  isto faremos que a busca de filmes que possuam uma dada
 *  palavra em seu nome nao seja 'case insensitive'. Para essa
 *  tarefa a funcao tolower() pode ser utilizada.
 *
 *  Esta funcao utiliza as funcoes freeST(), initST() e putFilmeST()
 *
 *  Para obter as palavras podemos escrever uma funcao peguePalavra()
 *  inspirada na funcao pegueNome do modulo lexer.c do EP3/EP4 ou
 *  ainda utilizar a funcao strtok() da biblioteca string.h:
 *  
 *      http://linux.die.net/man/3/strtok    (man page)
 *
 */
void
hashFilmes(ListaFilmes *lst)
{
    AVISO(hashFilmes em filmes.c: Vixe ainda nao fiz essa funcao...);
}

